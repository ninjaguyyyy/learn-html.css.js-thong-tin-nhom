## Về Project

- Ứng dụng kiến thức `html` và `css`.
- Thuộc môn học Lập trình ứng dụng Web.
- Chủ đề: Quản lý nhóm (thông tin nhóm, thành viên, chi tiết thành viên).

## Image Demo

1. Màn hình chính

   <img alt="anh 1" src="./assets/images/document/1.jpg"/>
   <img alt="anh 1" src="./assets/images/document/2.jpg"/>
   <img alt="anh 1" src="./assets/images/document/3.jpg"/>

2. Màn hình thành viên

   <img alt="anh 1" src="./assets/images/document/4.jpg"/>

## Kiến thức

`html`

- Sử dụng các thẻ cơ bản: p, img, div, ...
- Sử dụng **thẻ a** để **link các file html** khác nhau.
- Sử dụng **thẻ map** để quản lý từng khu vực trong ảnh (click mỗi người sẽ link tới mỗi trang)

`css`

- Style cho các tag cơ bản.
- Layout: dùng **float**
